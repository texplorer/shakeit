# ShakeIt

## Description
ShakeIt percussive instrument and play music

## Usage
Parameters that could changed by left wheel controller:
T - activation threshold, lower values mean its easier to activate ShakeIt
MT - minimal time in ms to play another sound. Lower values cause samples overlapping, and more metallic sound.
DT - delay time ms. Delay works quirky on this platform, so experiment with values.

## Authors and acknowledgment
Loading samples idea taken from Simple Drums by Anon

## License
MIT

## Project status
version 1
