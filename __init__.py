import math
import random
from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m.ui import colours
from st3m.ui.view import ViewManager
from st3m.goose import Optional, Generator
from ctx import Context
import bl00mbox
import leds

# SAMPLES_PATH = "/flash/sys/samples/"
SAMPLES_PATH = ""

SETTINGS_MODE_THRESHOLD = 0
SETTINGS_MODE_MIN_TIME = 1
SETTINGS_MODE_DELAY_TIME = 2
SETTINGS_POSITIONS = 3

class ShakeIt(Application):
    log = logging.Log(__name__, level=logging.INFO)

    channel: bl00mbox.Channel
    samples: list[bl00mbox.sampler] = []

    # sample_names: list[str] = [
    #     "kick.wav",
    #     "snare.wav",
    #     "hihat.wav",
    #     "crash.wav",
    #     "open.wav",
    #     "close.wav"
    # ]
    sample_names: list[str] = ["close.wav"] * 10
 

    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.a_y = 0
        self.a_x = 0
        self.a_z = 0
        self.threshold = 12.0
        self.power = 0.0
        self.act_sample = 0
        self.strength = 0
        self.last_strength = 0
        self.strengths = [0 for i in range(0,11)]
        self.fx_delay = None
        self.int_mixer = None
        self.last_shake_time = None
        self.amps = []
        self.settings_mode = SETTINGS_MODE_THRESHOLD
        self.min_time = 50
        self.last_button_time = 0
        self.delay_time = 0

        self.channel = bl00mbox.Channel("ShakeIt")
        self.channel.volume = 30000


    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self.int_mixer = self.channel.new(bl00mbox.plugins.mixer, 10)
        self.int_mixer.signals.gain = 4096

        self.fx_delay = self.channel.new(bl00mbox.plugins.delay)
        self.fx_delay.signals.time = self.delay_time
        self.fx_delay.signals.dry_vol = 30000
        self.fx_delay.signals.level = 16000
        self.fx_delay.signals.feedback = 16000
        self.fx_delay.signals.input = self.int_mixer.signals.output
        if self.fx_delay is not None:
            self.fx_delay.signals.output = self.channel.mixer
        else:
            self.int_mixer.signals.output = self.channel.mixer
    

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.is_loading:
            self._load_next_sample()
            return
        
        if self.last_shake_time is not None:
            self.last_shake_time += delta_ms
        self.last_button_time += delta_ms

        self.a_y = ins.imu.acc[0]
        self.a_x = ins.imu.acc[1]
        self.a_z =  ins.imu.acc[2]

        self.settings_change(ins)

        self.play()

    def settings_change(self, ins):
        if ins.buttons.app == ins.buttons.NOT_PRESSED:
            return
        if self.last_button_time < 300:
            return  # avoid quick value changes every think
        self.last_button_time = 0
        if ins.buttons.app == ins.buttons.PRESSED_DOWN:
            self.settings_mode = (self.settings_mode + 1) % SETTINGS_POSITIONS
        else:
            if self.settings_mode == SETTINGS_MODE_THRESHOLD:
                self.threshold = self.change_value(ins, self.threshold, 10, math.sqrt(100 + 100 + 200), 0.1)
            elif self.settings_mode == SETTINGS_MODE_MIN_TIME:
                self.min_time = self.change_value(ins, self.min_time, 0, 500, 10)
            elif self.settings_mode == SETTINGS_MODE_DELAY_TIME:
                delay_time = self.change_value(ins, self.delay_time, 0, 1000, 1)
                if self.delay_time != delay_time:
                    self.delay_time = delay_time
                    self.fx_delay.signals.time = self.delay_time

    def change_value(self,ins , value, min_limit, max_limit, step):
        if ins.buttons.app == ins.buttons.PRESSED_LEFT:
            if value > min_limit:
                value -= step
        elif ins.buttons.app == ins.buttons.PRESSED_RIGHT:
            if value < max_limit:
                value += step
        return value
    
    def play(self):
        # units of acceleration are m/s^2, so there is always gravity component about 10
        self.power = math.sqrt(self.a_y * self.a_y + self.a_x * self.a_x + self.a_z * self.a_z)
        if self.power > self.threshold and (self.last_shake_time is None or self.last_shake_time >= self.min_time):
            max_s = 10.0
            strength = self.power - self.threshold
            if strength > max_s:
                strength = max_s
            strength = strength / max_s
            self.strength = strength
            self.last_strength = strength
            self.strengths[self.act_sample] = strength
            self.last_shake_time = 0
            if self.act_sample < len(self.amps) and self.amps[self.act_sample]:
                self.amps[self.act_sample].signals.gain = 32767 * strength
            self.samples[self.act_sample].sample_start = random.randint(0, int(strength * 10000))
            self.samples[self.act_sample].signals.trigger.start()
            self.act_sample += 1
            if self.act_sample >= len(self.samples):
                self.act_sample = 0
        else:
            self.strength = 0


    def display_info(self, ctx, s, y):
        ctx.move_to(0, y)
        ctx.text(s)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(
            -120.0,
            -120.0,
            240.0,
            240.0,
        ).fill()

        ctx.save()
        ctx.rgb(*colours.GO_GREEN)
        ctx.text_baseline = ctx.MIDDLE 
        ctx.text_align = ctx.CENTER
        if self.is_loading:
            if len(self.samples) != len(self.sample_names):
                ctx.move_to(0, 0)
                ctx.font_size = 20
                ctx.font = "Camp Font 1"
                ctx.text(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")
                print(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")

            ctx.restore()
            return
        else:
            ctx.text_align = ctx.LEFT
            # logo
            ctx.font = "Camp Font 2"
            ctx.rgb(*colours.BLUE)
            ctx.font_size = 40
            ctx.move_to(-80, -50)
            ctx.text("Shake It")

            # animation
            ctx.font = "Camp Font 1"
            ctx.rgb(*colours.GO_GREEN)
            start_color = [1.0, 1.0, 1.0]
            end_color = [1.0, 0.1, 0.1]
            s = self.last_strength
            mixed_color = [v1 * (1.0 - s) + v2 * s for v1, v2 in zip(start_color, end_color)]
            ctx.rgb(*mixed_color)
            ctx.font_size = 40
            ctx.font = "Material Icons"
            ctx.move_to(-80, int(0 + s * 60))
            ctx.text("\ue8d0")  # circle
            ctx.move_to(-80, 80)
            ctx.text("\uea4a")  # head

            # parameters 
            ctx.rgb(*colours.GO_GREEN)
            ctx.font_size = 20
            ctx.font = "Camp Font 1"
            y = 0
            # self.display_info(ctx, f"x: {self.a_x:0.2f}", 0)
            # self.display_info(ctx, f"y: {self.a_y:0.2f}", 20)
            # self.display_info(ctx, f"z: {self.a_z:0.2f}", 40)
            prefix = "<> " if self.settings_mode == SETTINGS_MODE_DELAY_TIME else "" 
            self.display_info(ctx, f"{prefix}dt: {self.delay_time}", y)
            y += 20
            prefix = "<> " if self.settings_mode == SETTINGS_MODE_MIN_TIME else "" 
            self.display_info(ctx, f"{prefix}mt: {self.min_time}", y)
            y += 20
            prefix = "<> " if self.settings_mode == SETTINGS_MODE_THRESHOLD else "" 
            self.display_info(ctx, f"{prefix}t: {self.threshold:0.2f}", y)
            ctx.rgb(*colours.BLUE)
            y += 20
            self.display_info(ctx, f"p: {self.power:0.2f}", y)
            y += 20
            self.display_info(ctx, f"s: {self.last_strength:0.2f}", y)
            y += 20
            
            ctx.restore()

        # x,y,z acceleration
        offset = 4
        leds.set_rgb(offset + 8 - 2, self.a_x / 2.0 + 0.5, 0, 0)
        leds.set_rgb(offset + 8 + 2, self.a_x / 2.0 + 0.5, 0, 0)

        leds.set_rgb(offset + 16 - 2, 0.0, self.a_y / 2.0 + 0.5, 0.0)
        leds.set_rgb(offset + 16 + 2, 0.0, self.a_y / 2.0 + 0.5, 0.0)

        leds.set_rgb(offset + 32 - 2, 0.0, 0.0, self.a_z / 2.0 + 0.5)
        leds.set_rgb(offset + 32 + 2, 0.0, 0.0, self.a_z / 2.0 + 0.5)
        
        # rotating bling
        for i in range(0, 11):
            br = self.strengths[i]
            brs = br * 0.75
            leds.set_rgb(i * 4 - 1, brs, brs, brs)
            leds.set_rgb(i * 4, br, br, br)
            leds.set_rgb(i * 4 + 1, brs, brs, brs)
            # dim bling
            self.strengths[i] = self.strengths[i] * 0.4

        leds.update()


    def _load_next_sample(self) -> None:
        loaded = len(self.samples)
        if loaded >= len(self.sample_names):
            self.is_loading = False
            print(self.channel)
            return

        i = loaded

        sample = self.channel.new(bl00mbox.patches.sampler, SAMPLES_PATH + self.sample_names[i])
        print(sample)
        # sample.signals.output = self.channel.mixer
        # sample.signals.output = self.int_mixer.signals.mixer
        amp = self.channel.new(bl00mbox.plugins.ampliverter)
        amp.signals.input = sample.signals.output
        # setattr(self.int_mixer.signals, f"input{i}", sample.signals.output)
        setattr(self.int_mixer.signals, f"input{i}", amp.signals.output)

        # amp.signals.gain = self.plugins.env.signals.output

        self.amps.append(amp)
        self.samples.append(sample)
